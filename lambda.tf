locals {
	lambda-zip-location = "output/functionbasic.zip"
}
data "archive_file" "init" {
  type        = "zip"
  source_file = "functionbasic.py"
  output_path = local.lambda-zip-location
}

resource "aws_lambda_function" "test_lambda" {
  filename      = local.lambda-zip-location
  function_name = "functionbasic"
  role          = aws_iam_role.assume_role_policy.arn
  handler       = "functionbasic.hello"

  runtime = "python3.7"
}