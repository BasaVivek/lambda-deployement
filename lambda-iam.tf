resource "aws_iam_role_policy" "lambda-policy" {
  name = "test_policy"
  role = aws_iam_role.assume_role_policy.id

  policy = file("lambda-policy.json")
}
resource "aws_iam_role" "assume_role_policy" {
  name = "assume_role_policy"
  assume_role_policy = file("assume_role_policy.json")
}
 